<?php
/*
Plugin Name: Woocommerce custom post
Plugin URI: http://agilesolutionspk.com/
Description: This plug in has ability to create
 Woo-commerce make an offer custom post
Version: 1.0
Author URI: 
*/
if(!class_exists('Aspk_Wmao')){
   class Aspk_Wmao{
   function __construct(){
   add_action( 'init', array(&$this,'aspk_wmao_post' ));
   }
   
		function aspk_wmao_post() {
			$aspk_wmao_arr= array(
			  'public' => true,
			  'label'  => 'Order'
			  'taxonomies' => array('category')
			);
			register_post_type( 'order', $aspk_wmao_arr );
	
		}
		
	}
}
new Aspk_Wmao();


?>