<?php
/*
Plugin Name: Agile Make An Offer
Plugin URI: 
Description: This Plugin have Ability to Offer Price.
Author: Agile Solutions PK
Version: 1.0
Author URI: http://agilesolutionspk.com
*/

if ( !class_exists( 'Aspk_Wmao' )){

	class Aspk_Wmao{
		private $model;
		private $view;
		
		function __construct(){
			require_once(plugin_dir_path( __FILE__ ) .'view/view.php');
			require_once(plugin_dir_path( __FILE__ ) .'model/model.php');
			$this->model = new Aspk_Wmao_Model();
			$this->view  = new Aspk_Wmao_View();
			add_action( 'admin_menu', array(&$this, 'addAdminPage') );
		}
		function addAdminPage() {
			add_menu_page('Installment', 'Installment', 'manage_options', 'agile_installment_plan', array(&$this, 'agile_admin_show') );
		}
		function agile_admin_show(){
		
			echo "abc____";
			$this->view->new_data_show();
		}

	} //class ends
}//if class ends

$aspk_mk_an_offer = new Aspk_Wmao();
